#include "csapp.h"
#include "sbuf.h"
#include <time.h>
#include <unistd.h>

sbuf_t controller;

void *produce(void * argv);
void *consume(void *argv);

char received[10];
int tamanio;

int main(int argc, char **argv)
{
	char *filename;
	//int fd;
	//char c;
	struct stat fileStat;
	pthread_t consumidor, productor;

	sbuf_init(&controller, 10);

	if (argc != 2) {
		printf("usage: %s <filename>\n", argv[0]);
		exit(0);
	}
	filename = argv[1];


	if (stat(filename, &fileStat)<0){	
		printf("Archivo %s no fue encontrado\n",filename);
	}
	else{

		tamanio = fileStat.st_size;

		Pthread_create(&productor, NULL, produce, filename);
		Pthread_create(&consumidor, NULL, consume, NULL);
		
		Pthread_join(productor, NULL);
		Pthread_join(consumidor, NULL);

		// printf("Abriendo archivo %s...\n",filename);
		// fd = Open(filename, O_RDONLY, 0);
		// while(Read(fd,&c,1)){
		// 	sleep(0.05);
		// 	printf("Read: %c\n",c);
		// }			
		// Close(fd);
	}
	
	printf("controller buffer: %s\n", controller.buf);

	return 0;
}


void *produce(void * argv){

	char *nombre;
	int count = 0;
	char c;
	nombre = (char *) argv;

	if(tamanio < controller.n)
		controller.n = tamanio - 1;

	printf("Abriendo archivo %s...\n",nombre);

	int fd = Open(nombre, O_RDONLY, 0);

		while(1){
			
				Read(fd,&c,1);

				usleep(50000);
			
			
				sbuf_insert(&controller, c);
			

				printf("Read: %c\n",c);

				count++;

				printf("controller buffer: %s\n", controller.buf);
		}			
		Close(fd);

	return NULL;

}


void *consume(void *argv){

 	int count = 0;
 	char c;

 	while(1){
 		usleep(1000000);

 		c = sbuf_remove(&controller);

 		printf("Char: %c\n",c);

 		received[count] = c;

 		count++;
 	}

 	printf("Total read: %s\n", received);
	return NULL;

}
