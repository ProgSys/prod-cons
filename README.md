# Práctica 12: Problema Productor - Consumidor #

Aplicación con dos hilos, uno productor y otro consumidor. El productor lee un archivo de texto, extrae un carácter del archivo cada 50 ms y lo guarda en un buffer compartido. El hilo consumidor extrae un carácter del buffer compartido una vez por segundo y lo imprime en pantalla.

### Integrantes ###

* Jorge Cedeño
* Fabricio Layedra